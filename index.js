'use strict';

const crypto = require('crypto');
const fs = require('fs');
const hash = crypto.createHash('md5');
hash.setEncoding('hex');

const input = fs.createReadStream('./data/in.txt');
const output = fs.createWriteStream('./data/out.txt');

input.pipe(hash).pipe(process.stdout);
input.pipe(hash).pipe(output);


// Task 2
const hash1 = crypto.createHash('md5');
const input2 = fs.createReadStream('./data/in.txt');
const output2 = fs.createWriteStream('./data/out2');
const Transform = require('stream').Transform;

class CTransform extends Transform {

  _transform(data, encoding, callback) {
    data = data.toString('hex');
    console.log(data);
    callback(null, data);
  }
}
// в _transform обязателен вызов callback(). Этот метод напрямую в своем коде не вызывать. 
// Это должно производиться внутренними механизмами потока
// 2 способа: 
//  1.this.push(data)  запушиваем фрагмент данных chunk во внутренний буфер, а затем callback() 
//  2. Сразу вызов с аргументом  callback(data)Если данные таким образом не передать, то они будут отброшены 
// и на выходе потока ничего не будет.

input2
 .pipe(hash1)
 .pipe(new CTransform())
 .pipe(output2);

// Task 3
const stream = require('stream');

class CReadable extends stream.Readable {
  _read(size) {
    let num = (Math.floor(Math.random() * 100)).toString();
    this.push(num);
    //console.log(num);
  }
}
class CWritable extends stream.Writable {
  _write(data, encoding, callback) {
    console.log(data.toString());
    callback();
  }
}
class CTransform3 extends stream.Transform {
  _transform(data, encoding, callback) {
    setTimeout(() => {
      data = '+' + data;
    this.push(data);
    callback();
  }, 1000);
  }
}

const input3 = new CReadable({highWaterMark: 8});
const output3 = new CWritable();

input3
  .pipe(new CTransform3())
  .pipe(output3);